#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <linux/mxcfb.h>
#include <sys/mman.h>
#include <math.h>
#include <string.h>
#include <malloc.h>
#include "epdc-test.h"

#define WAVEFORM_MODE_INIT	0x0	/* Screen goes to white (clears) */
#define WAVEFORM_MODE_DU	0x1	/* Grey->white/grey->black */
#define WAVEFORM_MODE_GC16	0x2	/* High fidelity (flashing) */
#define WAVEFORM_MODE_GL16	0x3	/* High fidelity (no flashing) */
#define WAVEFORM_MODE_A2	0x6	/* Fast black/white animation */
#define WAVEFORM_MODE_DU4	0x7

#define EPDC_STR_ID "mxc_epdc_fb"

static int fb_fd;
static unsigned short *fb;
struct fb_var_screeninfo screen_info;
static int g_fb_size;
__u32 maker_val = 1;

static void memset_dword(void *s, int c, size_t count) {    
    for (int i = 0; i < count; i++)
        *((__u32 *)s + i) = c;
}

static int find_fb_dev(void) {
    int ret;
    struct fb_fix_screeninfo screen_info_fix;
    char fb_dev[10] = "/dev/fb";
    int fb_num = 0;

    while (1) {
        /* Scanning device, start with /dev/fb0 */
        fb_dev[7] = 0x30 + fb_num;
        fb_fd = open(fb_dev, O_RDWR, 0);
        if (fb_fd < 0) {
            printf("%s: Unable to open\n", fb_dev);
            return -1;
        }

        /* Check if is EPDC driver */
        ret = ioctl(fb_fd, FBIOGET_FSCREENINFO, &screen_info_fix);
        if (ret < 0) {
            printf("%s: Unable to read screen info\n", fb_dev);
            close(fb_fd);
            return -1;
        }

        /* Exit loop if EPDC is found */
        if (!strcmp(EPDC_STR_ID, screen_info_fix.id)) {
            printf("%s: Find EPDC device at: %s\n", __func__, fb_dev);
            break;
        }

        fb_num++;
        close(fb_fd);
    }

    return 0;
}

static int epdc_fbdev_setup(void) {
    int ret;

    ret = ioctl(fb_fd, FBIOGET_VSCREENINFO, &screen_info);
    if (ret < 0) return -1;
        
    printf("%s: Set background to 8-bpp\n", __func__);
	
    screen_info.bits_per_pixel = 8;
	screen_info.grayscale = 0;
	screen_info.yoffset = 0;
	screen_info.rotate = FB_ROTATE_UR;
	screen_info.activate = FB_ACTIVATE_FORCE;

    ret = ioctl(fb_fd, FBIOPUT_VSCREENINFO, &screen_info);
    if (ret < 0) return -1;

    /* Map the device to memory*/
    printf("%s: Memory-mapping framebuffer\n", __func__);
    g_fb_size = screen_info.xres_virtual * screen_info.yres_virtual * screen_info.bits_per_pixel / 8;
    fb = (unsigned short *)mmap(NULL, g_fb_size, PROT_READ | PROT_WRITE, MAP_SHARED, fb_fd, 0);
    if ((int)fb <= 0) {
        printf("%s: Failed to mapping FB device to memory\n", __func__);
        return -1;
    }

    return 0;
}

static int epdc_screen_setup(void) {
    int ret;
    int auto_update_mode;
    struct mxcfb_waveform_modes wv_modes;
    __u32 scheme = UPDATE_SCHEME_SNAPSHOT;
    __u32 pwrdown_delay = 0;

    printf("%s: Setup region update mode\n", __func__);
    auto_update_mode = AUTO_UPDATE_MODE_REGION_MODE;
    ret = ioctl(fb_fd, MXCFB_SET_AUTO_UPDATE_MODE, &auto_update_mode);
    if (ret < 0) {
        printf("%s: Failed to setup region update mode\n", __func__);
        return -1;
    }

	printf("%s: Setup waveform mode\n", __func__);
	wv_modes.mode_init = WAVEFORM_MODE_INIT;
	wv_modes.mode_du = WAVEFORM_MODE_DU;
	wv_modes.mode_gc4 = WAVEFORM_MODE_GC16;
	wv_modes.mode_gc8 = WAVEFORM_MODE_GC16;
	wv_modes.mode_gc16 = WAVEFORM_MODE_GC16;
	wv_modes.mode_gc32 = WAVEFORM_MODE_GC16;
    ret = ioctl(fb_fd, MXCFB_SET_WAVEFORM_MODES, &wv_modes);
    if (ret < 0) {
        printf("%s: Failed to setup waveform mode\n", __func__);
        return -1;
    }

    printf("%s: Setup update scheme\n", __func__);
    ret = ioctl(fb_fd, MXCFB_SET_UPDATE_SCHEME, &scheme);
    if (ret < 0) {
        printf("%s: Failed to setup update scheme\n", __func__);
        return -1;
    }

    printf("%s: Setup powerdown delay\n", __func__);
    ret = ioctl(fb_fd, MXCFB_SET_PWRDOWN_DELAY, &pwrdown_delay);
    if (ret < 0) {
        printf("%s: Failed to setup update scheme\n", __func__);
        return -1;
    }
    
    return 0;
}

static __u32 update_display_partial(int left, int top, int width, int height,
                                    int wave_mode, int wait_for_complete, unsigned int flags) {
    struct mxcfb_update_data update_data;
    struct mxcfb_update_marker_data update_marker_data;
    int ret;
    int wait = wait_for_complete | (flags & EPDC_FLAG_TEST_COLLISION);
    int max_retry = 10;

    update_data.dither_mode = 0;
	update_data.update_mode = UPDATE_MODE_PARTIAL;
	update_data.waveform_mode = wave_mode;
	update_data.update_region.left = left;
	update_data.update_region.width = width;
	update_data.update_region.top = top;
	update_data.update_region.height = height;
	update_data.temp = TEMP_USE_AMBIENT;
	update_data.flags = flags;

    if (wait) {
       update_data.update_marker = maker_val++;
    } else {
        update_data.update_marker = 0;
    }

    ret = ioctl(fb_fd, MXCFB_SEND_UPDATE, &update_data);
    while (ret < 0) {
        sleep(1);
        ret = ioctl(fb_fd, MXCFB_SEND_UPDATE, &update_data);
        if (--max_retry <= 0) {
            printf("%s: Max retries reached\n", __func__);
            wait = 0;
            flags = 0;
            break;
        }
    }

    if (wait) {
        update_marker_data.update_marker = update_data.update_marker;

        /* Wait for update to complete */
        ret = ioctl(fb_fd, MXCFB_WAIT_FOR_UPDATE_COMPLETE, &update_marker_data);
        if (ret < 0) {
            printf("%s: Wait for complete failed. Error = 0x%x\n", __func__, ret);
            flags = 0;
        }
    }

    if (flags & EPDC_FLAG_TEST_COLLISION) {
        printf("%s: Collision test result = %d\n", __func__, update_marker_data.collision_test);
        return update_marker_data.collision_test;
    }

    return update_data.waveform_mode;
}

static __u32 update_display_full(int wave_mode, int wait_for_complete, unsigned int flags) {
    struct mxcfb_update_data update_data;
    struct mxcfb_update_marker_data update_marker_data;
    int ret;
    int wait = wait_for_complete | (flags & EPDC_FLAG_TEST_COLLISION);
    int max_retry = 10;

	update_data.update_mode = UPDATE_MODE_FULL;
	update_data.waveform_mode = wave_mode;
	update_data.update_region.left = 0;
	update_data.update_region.width = screen_info.xres;
	update_data.update_region.top = 0;
	update_data.update_region.height = screen_info.yres;
	update_data.temp = TEMP_USE_AMBIENT;
	update_data.flags = flags;

    if (wait) {
       update_data.update_marker = maker_val++;
    } else {
        update_data.update_marker = 0;
    }

    ret = ioctl(fb_fd, MXCFB_SEND_UPDATE, &update_data);
    while (ret < 0) {
        sleep(1);
        ret = ioctl(fb_fd, MXCFB_SEND_UPDATE, &update_data);
        if (--max_retry <= 0) {
            printf("%s: Max retries reached\n", __func__);
            wait = 0;
            flags = 0;
            break;
        }
    }

    if (wait) {
        update_marker_data.update_marker = update_data.update_marker;

        /* Wait for update to complete */
        ret = ioctl(fb_fd, MXCFB_WAIT_FOR_UPDATE_COMPLETE, &update_marker_data);
        if (ret < 0) {
            printf("%s: Wait for complete failed. Error = 0x%x\n", __func__, ret);
            flags = 0;
        }
    }

    return update_data.waveform_mode;
}

static void copy_image_to_buffer(int left, int top, int width, int height, 
                                 unsigned int *img_ptr, struct fb_var_screeninfo *screen_info) {
    if ((width > screen_info->xres) || (height > screen_info->yres)) {
        printf("%s: Bad image dimension, force quit\n", __func__);
        return;   
    }

    for (int i = 0; i < height; i++)
        memcpy(fb + ((i + top) * screen_info->xres_virtual + left), img_ptr + (i * width), width * 2);
}

static void epdc_test_update(void)
{
    printf("%s: Starting test...\n", __func__);
    printf("\n%s: SCREEN INFO\n", __func__);
    printf("%s: Xres: %d - Yres: %d\n", __func__, screen_info.xres, screen_info.yres);
    printf("%s: Xres_vir: %d - Yres_vir: %d\n", __func__, screen_info.xres_virtual, screen_info.yres_virtual);
    printf("%s: height: %d - width: %d\n", __func__, screen_info.height, screen_info.width);

    printf("%s: Blank screen\n", __func__);
    memset(fb, 0xFF, screen_info.xres_virtual * screen_info.yres * screen_info.bits_per_pixel / 8);
    update_display_full(WAVEFORM_MODE_GC16, 1, 0);

    printf("%s: Update screen\n", __func__);
    memset(fb, 0xFF, screen_info.xres_virtual * screen_info.yres * screen_info.bits_per_pixel / 8);
    copy_image_to_buffer(0, 0, 1024, 758, xga_python_tutorial_0001_rgb_1024x758, &screen_info);
    update_display_full(WAVEFORM_MODE_GL16, 1, 0);
}

int main(int argc, char *argv) {
    int ret;

    ret = find_fb_dev();
    if (ret) goto err0;

    ret = epdc_fbdev_setup();
    if (ret) goto err1;

    ret = epdc_screen_setup();
    if (ret) goto err2;

    epdc_test_update();

err2:
    munmap(fb, g_fb_size);
err1:
    close(fb_fd);
err0:
    return ret;
}
